<!DOCTYPE html>
<%-- <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%> --%>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<meta charset="ISO-8859-1">
<title>Registration</title>
</head>
<body>


	<div class='navbar navbar-default'>
		<div class='container'>
			<a class='navbar-brand' href='/'>Multivision</a>

			<ul class='nav navbar-nav navbar-right'>
				<li><a href="/">Login in</a></li>
				<li><a href="register.jsp">Sign up</a></li>
			</ul>
		</div>
	</div>
	
<!-- 	<div class='col-md-3 col-md-offset-2'>
		<html:errors />
	</div> -->
	
	<div class='col-md-12'>
	</div>
	
	<form action="register.do" method="post" class="form-horizontal">

		<div class="form-group">
			<label class="col-md-2 control-label">User Name</label>
			<div class="col-md-3">
				<input type="text" class="form-control" name="username" placeholder="Email Address">
			</div>
		</div>



		<div class="form-group">
			<label class="col-md-2 control-label">First Name</label>
			<div class="col-md-3">
				<input type="text" class="form-control" name="customerFirstName"
					placeholder="First Name">
			</div>
		</div>

		<div class="form-group">
			<label for="last name" class="col-md-2 control-label">Last
				Name</label>
			<div class="col-md-3">
				<input type="text" class="form-control" name="customerLastName"
					placeholder="Last Name">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-2 control-label">Password</label>
			<div class="col-md-3">
				<input type="password" class="form-control" name="password1">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-2 control-label">Password Again</label>
			<div class="col-md-3">
				<input type="password" class="form-control" name="password2">
			</div>
		</div>

		<div class='col-sm-3 col-md-offset-2'>
			<input type="submit" value="Sign up" name="bank"
				class="btn btn-success">
		</div>
	</form>
</body>
</html>