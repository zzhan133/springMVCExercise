package net.multivision.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="/spitter")
public class SpitterExample {
	
	private static final Logger logger = LoggerFactory.getLogger(SpitterExample.class);
	@RequestMapping
	public String example(Model model){
		logger.info("get spitter Edit page");
		return "newEdit";
	}
}
