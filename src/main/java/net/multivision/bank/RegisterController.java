package net.multivision.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RegisterController {
	
	private static final Logger logger = LoggerFactory.getLogger(RegisterController.class);
	
	@RequestMapping(value="/register", method = RequestMethod.GET)
	public String register(){
		logger.info("Request for register page");
		return "register";
	}
	
	@RequestMapping(value="/add", method = RequestMethod.POST, params="new")
	public String addNewCustomer(){
		return "home";
	}
}
